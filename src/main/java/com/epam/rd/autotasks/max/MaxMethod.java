package com.epam.rd.autotasks.max;

public class MaxMethod {

    public static int max(int[] values) {
        int maxNumber = values[0];
        for (int numbers : values) {
            if (numbers > maxNumber)
                maxNumber = numbers;
        }
        return maxNumber;
    }
}
